class Village:
    buldings = 0
    residents = 0    
    
    def __str__ (self):
        return f"Village have a {self.buldings} buldings, {self.elfs} Elfs, {self.renifers} Renifers"

class Bulding(Village):
    def __init__ (self, street_name, street_number):
        self.street_name = street_name
        self.street_number = street_number
        Village.buldings += 1
    
class Factory(Bulding):
    
    def __init__(self, street_name, street_number, name_factory, max_workers, product):
        super().__init__(street_name, street_number)
        self.name_factory = name_factory
        self.max_workers = max_workers
        self.product = product
        self.worker = []
        print(f"{street_name} {street_number} new {name_factory}, max workers {max_workers}, final product {product}")
    
    def __str__ (self):
        return f"Name of bulding {self.name_factory}, max workers {self.max_workers}, final product {self.product}"

    def elf_work(self, elf):
        if len(self.worker) < self.max_workers:
            self.worker.append(elf.name_elf)
            elf.work_factory = self.name_factory
            return print(f"New worker in {self.name_factory}, working {len(self.worker)} elfs, wokrers: {self.worker}")
        return print(f"There are no vacancies.")

class SantaHouse (Bulding):
    santa_house = 0
    mailbox = 0
    def __init__ (self, street_name, street_number):
        super().__init__(street_name, street_number)
        if SantaHouse.santa_house == 0:
            SantaHouse.santa_house += 1
            print(f"Santa's house is located on the street: {self.street_name} {self.street_number}")
        else:
            print("There can only be one Santa's house")

    def new_letter (self, wish):
        SantaHouse.mailbox += 1
        self.wish = wish
        print(f"Naw wish letter: {self.wish}") 

def Shop(Bulding):
    total_cash = 0

    def __init__ (self, street_name, street_numer, shop_name, totel_amount_products):
        super().__init__(street_name, street_numer)
        self.shop_name = shop_name
        slef.total_amount_products = total_amount_products

    def sell (self, amount_products, total_price):
        self.amount_products = amount_products
        self.total_price = total_price
        self.total_cash = self.total_cash + self.total_price
        self.total_amount_products = self.total_amount_products - self.amount_products
        return self.total_cash
    
class Resident(Village):
    def __init__ (self, name, age):
        self.name = name
        self.age = age
        Village.elfs += 1
        
class Elf(Resident):
    work_factory = "unemployed"
    budget = 0
    vitality = 100
    
    def __init__ (self, name, age):
        super().__init__(name, age)   
        
        
    def __str__(self):
        return f"New Elf name {self.name_elf}, {self.age} yerars old, worik in {self.work_factory}, vitality: {self.vitality}, budget: {self.budget}"

    def go_work (self):
        if self.vitality <= 10:
            print("You don't have enough vitality to work.")
        else:
            self.budget += 100
            self.vitality -= 10
            
    def eat (self):
        if self.vitality >= 100:
            print("You are full.")
            self.vitality = 100
        else:
            self.budget -= 100
            self.vitality += 20            

class Renifer(Resident):
    def __init__(self, name, age, weight, speed):
        super().__init__(name, age)
        self.weight = weight
        self.speed = speed
        
    def __str__(self):
        return f"{self.name_renifer} weight: {self.weight}, speed: {self.speed} km/h"
    
    def new_speed(self, new_speed):
        self.new_speed = new_speed
        self.speed = new_speed

    def feed(self):
        self.weight += 10
        self.speed -= 5

santa_home = SantaHouse("Polart st", 10)
print(Village.buldings)
factory1 = Factory("Polar st", 10, "Toys Factory", 100, "Toys")
print(Village.buldings)
